<?php
namespace Vbudnik\DynamicallyProductView\Plugin\Magento\ConfigurableProduct\Product\View\Type;

use Magento\Framework\Json\EncoderInterface;
use Magento\Framework\Json\DecoderInterface;

class Configurable
{
	protected $jsonEncoder;
	protected $jsonDecoder;
	protected $_productRepository;

	public function __construct(
		\Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
		EncoderInterface $jsonEncoder,
		DecoderInterface $jsonDecoder
	) {
		$this->jsonDecoder = $jsonDecoder;
		$this->jsonEncoder = $jsonEncoder;
		$this->_productRepository = $productRepository;
	}

	public function getProductById($id)
	{
		return $this->_productRepository->getById($id);
	}

	public function aroundGetJsonConfig(
		\Magento\ConfigurableProduct\Block\Product\View\Type\Configurable $subject,
		\Closure $proceed
	)
	{
		$sname = [];
		$sdescription = [];

		$config = $proceed();
		$config = $this->jsonDecoder->decode($config);

		foreach ($subject->getAllowProducts() as $prod) {
			$id = $prod->getId();
			$product = $this->getProductById($id);
			$sname[$id] = $product->getName();
			$sdescription[$id] = $product->getDescription();
		}
		$config['sname'] = $sname;
		$config['sdescription'] = $sdescription;

		return $this->jsonEncoder->encode($config);
	}
}
