<?php
/**
 * Created by PhpStorm.
 * User: a4min
 * Date: 11.08.17
 * Time: 12:15
 */

namespace Aheadworks\Layerednav\Controller\Adminhtml\Sorter;


class Index extends \Magento\Backend\App\Action
{
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $_resultPageFactory;

    /**
     * Index constructor.
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $pageFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $pageFactory
    )
    {
        $this->_resultPageFactory = $pageFactory;
        parent::__construct($context);
    }

    /**
     * Init page
     *
     * @param \Magento\Backend\Model\View\Result\Page $resultPage
     * @return \Magento\Backend\Model\View\Result\Page
     */
    protected function initPage($resultPage)
    {
        $resultPage->setActiveMenu('Aheadworks_Layerednav:attributes_options_sorter');
        return $resultPage;
    }

    /**
     * Check the permission to run it
     *
     * @return boolean
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Aheadworks_Layerednav::attributes_options_sorter');
    }

    public function execute()
    {
        $resultPage = $this->_resultPageFactory->create();
        $resultPage->addBreadcrumb(__('Attributes'), __('Attributes'))
            ->addBreadcrumb(__('Options sorter'), __('Options sorter'));
        $resultPage->getConfig()->getTitle()->prepend(__('Options sorter'));

        $this->_view->renderLayout();
    }

}