<?php
/**
 * Created by PhpStorm.
 * User: a4min
 * Date: 11.08.17
 * Time: 14:14
 */

namespace Aheadworks\Layerednav\Controller\Adminhtml\Sorter;


class Sorter extends \Magento\Backend\App\Action
{
    const CONTROL_VALUE_COUNT = 3;
    const PRODUCT_ENTITY_TYPE_ID = 4;

    /**
     * @var \Magento\Framework\Controller\Result\Json $resultJson
     */
    private $_resultJson;

    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    private $_resourceConnection;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $_storeManager;

    private $_connection;
    private $_errors = [];
    private $_attributesCount = 0;
    private $_status = false;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Controller\Result\Json $json,
        \Magento\Framework\App\ResourceConnection $resourceConnection,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
        $this->_resultJson = $json;
        $this->_resourceConnection = $resourceConnection;
        $this->_storeManager = $storeManager;
        parent::__construct($context);
    }

    public function execute()
    {
        $entityTypeId = self::PRODUCT_ENTITY_TYPE_ID;
        $select = $this->getAttributeSelect();
        if ($attributeSetId = $this->getRequest()->getPostValue('attribute_set_id')) {
            $select->joinLeft(
                    ['attr_set' => $this->getTableName('eav_entity_attribute')],
                    'attr.attribute_id = attr_set.attribute_id',
                    []
                )->where("attr.entity_type_id = {$entityTypeId} AND attr_set.attribute_set_id = {$attributeSetId}");
            $attributes = $this->getConnection()->fetchAll($select);
        } elseif ($attributeId = $this->getRequest()->getPostValue('attribute_id')) {
            $select->where("attr.attribute_id = {$attributeId}");
            $attributes = $this->getConnection()->fetchAll($select);
        } else {
            $select->where("entity_type_id = {$entityTypeId}");
            $attributes = $this->getConnection()->fetchAll($select);
        }

        foreach ($attributes as $attribute) {
            $this->_attributesCount++;
            $this->sortAttributeOptions($attribute);
        }

        $this->_status = true;
        return $this->getResult();
    }

    /**
     * @return \Magento\Framework\DB\Select
     */
    private function getAttributeSelect() {
        $select = $this->getConnection()->select()
            ->from(
                ['attr' => $this->getTableName('eav_attribute')],
                ['attribute_id','attribute_code']
            )->joinInner(
                ['cea' => $this->getTableName('catalog_eav_attribute')],
                'attr.attribute_id = cea.attribute_id AND cea.is_filterable = 1',
                []
            );
        return $select;
    }

    private function getResult()
    {
        return $this->_resultJson->setData([
            'status' => $this->_status,
            'errors' => $this->_errors,
            'attributesCount'=> $this->_attributesCount
        ]);
    }

    private function getConnection () {
        if (!$this->_connection) {
            $this->_connection = $this->_resourceConnection->getConnection();
        }
        return $this->_connection;
    }

    private function getTableName($name)
    {
        return $this->_resourceConnection->getTableName($name);
    }

    private function sortAttributeOptions($attribute) {
        $attributeId = $attribute['attribute_id'];
        $attributeCode = $attribute['attribute_code'];
        $DF = substr($attributeCode,0,2) == 'DF';
        $controlValues = $this->getConnection()->fetchAll($this->getAttributeOptionsSelect($attributeId)->limit(self::CONTROL_VALUE_COUNT));
        if ($this->isOptionsWithNumbers(array_column($controlValues,'value')) && !$DF) {
            $select = $this->getAttributeOptionsSelect($attributeId)->order('LPAD(lower(value), 10,0) ASC');
        } else {
            $select = $this->getAttributeOptionsSelect($attributeId)->order('value ASC');
        }

        $options = $this->getConnection()->fetchAll($select);

        if ($DF) {
            $newOptions = [];
            $extremeValues = [];
            foreach ($options as $key => $option) {
                preg_match_all('/[\d,.]+/', $option['value'], $matches);
                if (!empty($matches) && !empty($matches[0])) {
                    $newKey = $matches[0][0] * 1000;
                    while (isset($newOptions[$newKey])) {
                        if (isset($matches[0][1])) {
                            $newKey = $newKey + (int)$matches[0][1];
                        } else {
                            $newKey = $newKey - 1;
                        }
                    }
                    $newOptions[$newKey] = $option;
                } else {
                    $newOptions[$option['value']] = $option;
                }
            }

            ksort($newOptions);
            $options = array_values($newOptions);
        }

        foreach ($options as $key => $option) {
            try {
                $this->getConnection()->update(
                    $this->getTableName('eav_attribute_option'),
                    ['sort_order' => $key * 100],
                    "option_id = {$option['option_id']}"
                );
            } catch (\Exception $e) {
                $this->_errors[] = "option_id = {$option['option_id']} | " . $e->getMessage();
            }
        }
    }

    /**
     * @param array $values
     * @return bool
     */
    private function isOptionsWithNumbers($values)
    {
        $result = 0;
        foreach ($values as $value) {
            if (preg_match('~[0-9]~', $value)) {
                $result++;
            }
        }
        return $result == self::CONTROL_VALUE_COUNT;
    }

    /**
     * @param $attributeId
     * @return \Magento\Framework\DB\Select
     */
    private function getAttributeOptionsSelect($attributeId)
    {
        $storeId = $this->_storeManager->getDefaultStoreView()->getId();
        $select = $this->getConnection()->select()
            ->from(
                ['values_table' => $this->_resourceConnection->getTableName('eav_attribute_option_value')],
                ['option_id', 'value']
            )->joinLeft(
                ['option_table' => $this->getTableName('eav_attribute_option')],
                'option_table.option_id = values_table.option_id',
                []
            )->where(
                "store_id IN ( {$storeId} ,0) AND attribute_id = {$attributeId}"
            )->group('option_id');
        return $select;
    }

}