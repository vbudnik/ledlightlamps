<?php
/**
 * Created by PhpStorm.
 * User: a4min
 * Date: 11.08.17
 * Time: 13:08
 */

namespace Aheadworks\Layerednav\Controller\Adminhtml\Sorter;


class AutocompleteAttributes extends \Magento\Backend\App\Action
{
    /**
     * @var \Magento\Framework\Controller\Result\Json $resultJson
     */
    private $_resultJson;

    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    private $_resourceConnection;

    private $entityTypeId = Sorter::PRODUCT_ENTITY_TYPE_ID;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Controller\Result\Json $json,
        \Magento\Framework\App\ResourceConnection $resourceConnection
    ) {
        $this->_resultJson = $json;
        $this->_resourceConnection = $resourceConnection;
        parent::__construct($context);
    }

    public function execute()
    {
        if($string = $this->getRequest()->getPostValue('string')){
            try {
                $connection = $this->_resourceConnection->getConnection();
                $select = $connection->select()
                    ->from(
                        $this->_resourceConnection->getTableName('eav_attribute'),
                        [
                            'value' => 'attribute_code',
                            'attribute_id' => 'attribute_id'
                        ]
                    )->where("attribute_code LIKE '%{$string}%' AND entity_type_id = {$this->entityTypeId}");
                $response = $connection->fetchAll($select);
            } catch (\Exception $e) {
                $response = ['error' => 'true', 'message' => $e->getMessage()];
            }
            return $this->_resultJson->setData($response);
        }
        return $this->_resultJson->setData(['error' => 'true', 'message' => 'Empty data']);
    }
}