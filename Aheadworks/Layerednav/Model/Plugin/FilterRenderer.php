<?php
namespace Aheadworks\Layerednav\Model\Plugin;

use Aheadworks\Layerednav\Block\Navigation\Swatches\FilterRenderer as SwatchesFilterRenderer;
use Magento\Catalog\Model\Layer\Filter\FilterInterface;
use Magento\Framework\View\LayoutInterface;
use Magento\Swatches\Helper\Data as SwatchesHelper;

/**
 * Class FilterRenderer
 * @package Aheadworks\Layerednav\Model\Plugin
 */
class FilterRenderer
{
    /**
     * @var LayoutInterface
     */
    private $layout;

    /**
     * @var SwatchesHelper
     */
    private $swatchHelper;

    /**
     * @param LayoutInterface $layout
     * @param SwatchesHelper $swatchHelper
     */
    public function __construct(
        LayoutInterface $layout,
        SwatchesHelper $swatchHelper
    ) {
        $this->layout = $layout;
        $this->swatchHelper = $swatchHelper;
    }

    /**
     * @param \Aheadworks\Layerednav\Block\Navigation\FilterRenderer $subject
     * @param \Closure $proceed
     * @param FilterInterface $filter
     * @return string
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function aroundRender(
        \Aheadworks\Layerednav\Block\Navigation\FilterRenderer $subject,
        \Closure $proceed,
        FilterInterface $filter
    ) {
        if ($filter->hasAttributeModel()) {
            if ($this->swatchHelper->isSwatchAttribute($filter->getAttributeModel())) {
                /** @var SwatchesFilterRenderer $swatchFilterRenderer */
                $swatchFilterRenderer = $this->layout->createBlock(SwatchesFilterRenderer::class);
                return $swatchFilterRenderer->setSwatchFilter($filter)->toHtml();
            }
        }
        return $proceed($filter);
    }
}
