<?php
namespace Aheadworks\Layerednav\Model\Plugin;

use Aheadworks\Layerednav\Model\PageTypeResolver;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\View\LayoutInterface;

/**
 * Class Result
 * @package Aheadworks\Layerednav\Model\Plugin
 */
class Result
{
    const PROCESS_OUTPUT_FLAG = 'aw_layered_nav_process_output';

    /**
     * @var RequestInterface|\Magento\Framework\App\Request\Http
     */
    private $request;

    /**
     * @var LayoutInterface
     */
    private $layout;

    /**
     * @var PageTypeResolver
     */
    private $pageTypeResolver;

    /**
     * @param RequestInterface $request
     * @param LayoutInterface $layout
     * @param PageTypeResolver $pageTypeResolver
     */
    public function __construct(
        RequestInterface $request,
        LayoutInterface $layout,
        PageTypeResolver $pageTypeResolver
    ) {
        $this->request = $request;
        $this->layout = $layout;
        $this->pageTypeResolver = $pageTypeResolver;
    }

    /**
     * @param ResultInterface $result
     * @param \Closure $proceed
     * @param ResponseInterface $response
     * @return ResultInterface
     */
    public function aroundRenderResult(
        ResultInterface $result,
        \Closure $proceed,
        ResponseInterface $response
    ) {

        try {
            if ($this->request->isAjax() && $this->request->getParam(self::PROCESS_OUTPUT_FLAG)) {

                if ($block = $this->layout->getBlock('catalog.leftnav')) {
                    $block->setTemplate('Aheadworks_Layerednav::layer/view.phtml');
                }

                if ($this->pageTypeResolver->getType() == PageTypeResolver::PAGE_TYPE_CATEGORY) {
                    $navigationBlockName = 'catalog.leftnav';
                } elseif ($this->pageTypeResolver->getType() == PageTypeResolver::PAGE_TYPE_CATALOG_SEARCH) {
                    $navigationBlockName = 'catalogsearch.leftnav';
                } elseif ($this->pageTypeResolver->getType() == PageTypeResolver::PAGE_TYPE_PROMO) {
                    $navigationBlockName = 'promo.leftnav';
                }

                /** @var \Magento\Framework\App\Response\Http $response */
                $response->setBody(
                    \Zend_Json::encode(
                        [
                            'mainColumn' => $this->layout->renderElement('main'),
                            'navigation' => $this->layout->renderElement($navigationBlockName)
                        ]
                    )
                );

                return $result;

            }
            /*elseif ($this->request->getParam('p', false)) {
                if ($block = $this->layout->getBlock('category.products.list')) {
                    $block->setTemplate('Aheadworks_Layerednav::product/list.phtml');
                }
            }*/
            /*else {
                if ($block = $this->layout->getBlock('category.products.list')) {
                    $block->setTemplate('Aheadworks_Layerednav::product/list.phtml');
                }
            }*/
        } catch (\Exception $e) {
            $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/Layerednav_Plugin_errors.log');
            $logger = new \Zend\Log\Logger();
            $logger->addWriter($writer);
            $logger->info(print_r($e->getMessage(),true));
        }

        return $proceed($response);
    }
}
