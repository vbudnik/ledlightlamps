<?php
namespace Aheadworks\Layerednav\Model;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;

/**
 * Layered Navigation config
 */
class Config
{
    /**
     * Configuration path to display 'New' filter flag
     */
    const XML_PATH_NEW_FILTER_ENABLED = 'aw_layerednav/general/display_new';

    /**
     * Configuration path to display 'On Sale' filter flag
     */
    const XML_PATH_ON_SALE_FILTER_ENABLED = 'aw_layerednav/general/display_sales';

    /**
     * Configuration path to display 'In Stock' filter flag
     */
    const XML_PATH_STOCK_FILTER_ENABLED = 'aw_layerednav/general/display_stock';

    /**
     * Configuration path to Enable AJAX flag
     */
    const XML_PATH_AJAX_ENABLED = 'aw_layerednav/general/enable_ajax';

    /**
     * Configuration path to Enable AJAX flag
     */
    const XML_PATH_POPOVER_DISABLED = 'aw_layerednav/general/disable_popover';

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(ScopeConfigInterface $scopeConfig)
    {
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * Check if 'New' filter enabled
     *
     * @return bool
     */
    public function isNewFilterEnabled()
    {
        return $this->scopeConfig->isSetFlag(
            self::XML_PATH_NEW_FILTER_ENABLED,
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * Check if 'On Sale' filter enabled
     *
     * @return bool
     */
    public function isOnSaleFilterEnabled()
    {
        return $this->scopeConfig->isSetFlag(
            self::XML_PATH_ON_SALE_FILTER_ENABLED,
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * Check if 'In Stock' filter enabled
     *
     * @return bool
     */
    public function isInStockFilterEnabled()
    {
        return $this->scopeConfig->isSetFlag(
            self::XML_PATH_STOCK_FILTER_ENABLED,
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * Check if AJAX on storefront enabled
     *
     * @return bool
     */
    public function isAjaxEnabled()
    {
        return $this->scopeConfig->isSetFlag(self::XML_PATH_AJAX_ENABLED);
    }

    /**
     * Check if "Show X Items" Pop-over disabled
     *
     * @return bool
     */
    public function isPopoverDisabled()
    {
        return $this->scopeConfig->isSetFlag(self::XML_PATH_POPOVER_DISABLED);
    }
}
