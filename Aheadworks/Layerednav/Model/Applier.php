<?php
namespace Aheadworks\Layerednav\Model;

use Aheadworks\Layerednav\Model\Layer\FilterListResolver;
use Aheadworks\Layerednav\Model\ResourceModel\Layer\ConditionRegistry;
use Magento\Catalog\Model\Layer;
use Magento\Framework\App\RequestInterface;

/**
 * Class Applier
 * @package Aheadworks\Layerednav\Model
 */
class Applier
{
    /**
     * @var FilterListResolver
     */
    private $filterListResolver;

    /**
     * @var RequestInterface
     */
    private $request;

    /**
     * @var ConditionRegistry
     */
    private $conditionRegistry;

    private $_seoData;
    private $_filterSettings;

    /**
     * Applier constructor.
     * @param FilterListResolver $filterListResolver
     * @param RequestInterface $request
     * @param ConditionRegistry $conditionRegistry
     * @param \Project\SeoFilters\Model\FilterSettingsFactory $filterSettings
     */
    public function __construct(
        FilterListResolver $filterListResolver,
        RequestInterface $request,
        ConditionRegistry $conditionRegistry,
        \Project\SeoFilters\Model\FilterSettingsFactory $filterSettings
    ) {
        $this->filterListResolver = $filterListResolver;
        $this->request = $request;
        $this->conditionRegistry = $conditionRegistry;
        $this->_filterSettings = $filterSettings;
    }

    /**
     * Apply filters
     *
     * @param Layer $layer
     * @return void
     */
    public function applyFilters(Layer $layer)
    {
        $params = $this->request->getParams();
        if (isset($params['id']) && count($params) < 2) {
            //return false;
        }

        if ($this->request->getParam('price',false)) {
            $params['aw_stock'] = '1';
            $params['filterValue'][] = [
                "key" => "aw_stock",
                "value" => "1"
            ];

            $this->request->setParams($params);
        }

        $filterList = $this->filterListResolver->get();
        $filters = $filterList->getFilters($layer);

        foreach ($filters as $filter) {
            $filter->apply($this->request);
        }

        $collection = $layer->getProductCollection();
        $whereConditions = $this->conditionRegistry->getConditions();
        if ($whereConditions) {
            foreach ($whereConditions as $conditions) {
                $condition = implode(' OR ', $conditions);
                $collection->getSelect()->where("({$condition})");
            }
            $collection->getSize();
            $collection->getSelect()->group('e.entity_id');
        }

        if ($this->request->getModuleName() == 'catalog')
        $this->applySeoData($layer, $filters);

        $layer->apply();
    }

    protected function getSeoLink($layer, $pageFilter, $pageFilterItem)
    {
        /*$stateFilters = $layer->getState()->getFilters();

        if (!empty($stateFilters)) {
            return $pageFilterItem->getUrl(false);
        }*/

        if (!$pageFilterAttributeModel = $pageFilter->getData('attribute_model')) return '';

        $link = sprintf("%s/%s-%s/%s/",
            trim(strtok($pageFilterItem->getUrl(), '?'), '/'),
            $pageFilterAttributeModel->getData('attribute_code'),
            $pageFilterItem->getValue(),
            urlencode(strtolower(str_replace(' ','_',$pageFilterItem->getLabel())))
        );

        return $link;
    }

    protected function applySeoData(Layer $layer, $filters)
    {
        if (!$categoryId = $layer->getCurrentCategory()->getId()) return false;

        $seoFilters = [];
        /** @var \Project\SeoFilters\Model\FilterSettings $filterSettings */
        $filterSettings = $this->_filterSettings->create();
        $filterCollection = $filterSettings->getCollection()
            ->addFieldToFilter('category_id', $categoryId);
        foreach ($filterCollection as $seoItem) {
            $seoFilters[$seoItem['attribute_id']][$seoItem['value_id']] = $seoItem;
        }

        foreach ($filters as $pageFilter) {
            if (!$pageFilterAttributeModel = $pageFilter->getData('attribute_model')) continue;
            if (!$pageFilterAttributeId = $pageFilterAttributeModel->getData('attribute_id')) continue;

            if (isset($seoFilters[$pageFilterAttributeId])) {
                foreach ($pageFilter->getItems() as $pageFilterItem) {
                    if (isset($seoFilters[$pageFilterAttributeId][$pageFilterItem->getValue()])) {
                        $pageFilterItem->setData('seo_link', $this->getSeoLink($layer, $pageFilter, $pageFilterItem));
                    }
                }
            }
        }

        $stateFilters = $layer->getState()->getFilters();

        if (count($stateFilters) != 1) return false;

        $filterValues = explode(',', $stateFilters[0]->getValueString());

        if (count($filterValues) != 1) return false;

        $filterAttributeId = $stateFilters[0]->getFilter()->getData('attribute_model')->getData('attribute_id');
        $filterValueId = $filterValues[0];

        if (isset($seoFilters[$filterAttributeId][$filterValueId])) {
            $seoFilterItem = $seoFilters[$filterAttributeId][$filterValueId];
            $this->setSeoData('page_title', $seoFilterItem['seo_page_title']);
            $this->setSeoData('page_description', $seoFilterItem['seo_page_description']);
            $this->setSeoData('breadcrumb_title', $seoFilterItem['seo_breadcrumb_title']);
            $this->setSeoData('page_h1_title', $seoFilterItem['seo_h1_title']);
            $layer->getCurrentCategory()->setData('seo_category_description', $seoFilterItem['seo_category_description']);
            $layer->getCurrentCategory()->setData('seo_breadcrumb_title', $seoFilterItem['seo_breadcrumb_title']);
            $layer->getCurrentCategory()->setData('seo_h1_title', $seoFilterItem['seo_h1_title']);
        }

    }

    protected function setSeoData($key, $value) {
        $this->_seoData[$key] = $value;
    }

    public function getSeoData($key) {
        return isset($this->_seoData[$key]) ? $this->_seoData[$key] : false;
    }

}
