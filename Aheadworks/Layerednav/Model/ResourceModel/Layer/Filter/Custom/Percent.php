<?php
namespace Aheadworks\Layerednav\Model\ResourceModel\Layer\Filter\Custom;

use Magento\Catalog\Model\Layer\Filter\FilterInterface;
use Magento\Catalog\Model\ResourceModel\Product\Collection;
use Magento\Framework\DB\Select;

/**
 * Class Stock
 * @package Aheadworks\Layerednav\Model\ResourceModel\Layer\Filter\Custom
 */
class Percent extends AbstractFilter
{

    /**
     * {@inheritdoc}
     */
    protected function getDateFromAttrCode()
    {
        return '';
    }

    /**
     * {@inheritdoc}
     */
    protected function getDateToAttrCode()
    {
        return '';
    }

    /**
     * {@inheritdoc}
     */
    public function getWhereConditions(FilterInterface $filter, $value)
    {
        $tableAlias = 'landing_item';
        $values = explode(',',$value);
        $result = [];
        foreach ($values as $value) {
            $result[$tableAlias][] = "{$tableAlias}.discount_range = '{$value}'";
        }
        return $result;
    }

    /**
     * {@inheritdoc}
     */
    protected function getSpecialConditions(FilterInterface $filter, $value)
    {
        return [];
    }
}
