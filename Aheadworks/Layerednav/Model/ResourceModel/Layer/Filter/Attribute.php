<?php
namespace Aheadworks\Layerednav\Model\ResourceModel\Layer\Filter;

use Magento\Catalog\Model\Layer\Filter\FilterInterface;
use Magento\Framework\DB\Select;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

/**
 * Class Attribute
 * @package Aheadworks\Layerednav\Model\ResourceModel\Layer\Filter
 */
class Attribute extends AbstractDb
{

    const LEFT_JOIN_TO_EXCLUDE = [
        'name_attr_value',
        'inventory_table',
        'day_discount_price',
        'top',
        'compare',

//        'stock_status_index',
        //'price_index',
        'name_attr_code'
    ];

    protected $_cacheManager;

    protected $storeManager;
    private $_filterCache;

    /**
     * {@inheritdoc}
     */
    protected function _construct()
    {
        $this->_init('catalog_product_index_eav', 'entity_id');
    }

    public function __construct(
        \Magento\Framework\Model\ResourceModel\Db\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\App\CacheInterface $cacheManager
    ) {
        parent::__construct($context);
        $this->storeManager = $storeManager;
        $this->_cacheManager = $cacheManager;
    }

    /**
     * @param FilterInterface $filter
     * @throws \Magento\Framework\Exception\LocalizedException
     * @return $this
     */
    public function joinFilterToCollection(FilterInterface $filter)
    {
        $collection = $filter->getLayer()->getProductCollection();
        $attribute = $filter->getAttributeModel();
        $connection = $this->getConnection();
        $tableAlias = $attribute->getAttributeCode() . '_idx';
        $flag = 'attribute_joined_' . $tableAlias;
        if (!$collection->getFlag($flag)) {
            $conditions = [
                $tableAlias . '.entity_id = e.entity_id',
                $connection->quoteInto($tableAlias . '.attribute_id = ?', (int)$attribute->getAttributeId()),
                $connection->quoteInto($tableAlias . '.store_id = ?', (int)$collection->getStoreId()),
            ];

            $collection->getSelect()->joinLeft(
                [$tableAlias => $this->getMainTable()],
                implode(' AND ', $conditions),
                []
            );
            $collection->setFlag($flag, true);
        }

        return $this;
    }

    /**
     * Get where condition
     *
     * @param FilterInterface $filter
     * @param string|array $value
     * @return array
     */
    public function getWhereConditions(FilterInterface $filter, $value)
    {
        if (is_string($value)) {
            $value = explode(',', $value);
        }
        $attribute = $filter->getAttributeModel();
        $connection = $this->getConnection();
        $tableAlias = $attribute->getAttributeCode() . '_idx';
        return [$connection->quoteInto($tableAlias . '.value IN (?)', $value)];
    }

    /**
     * Retrieve array with products counts per attribute option
     *
     * @param FilterInterface $filter
     * @throws \Magento\Framework\Exception\LocalizedException
     * @return array
     */
    public function getCount(FilterInterface $filter)
    {
        // Clone select from collection with filters
        $select = clone $filter->getLayer()->getProductCollection()->getSelect();
        // Remove where conditions for current attribute - compatibility with multiselect attributes
        $whereConditions = $select->getPart(Select::WHERE);
        $select->reset(Select::WHERE);
        $select->reset(Select::GROUP);

        $attribute = $filter->getAttributeModel();
        $tableAlias = sprintf('%s_idx', $attribute->getAttributeCode());

        foreach ($whereConditions as $key => $condition) {
            if (false !== strpos($condition, $tableAlias)) {
                unset($whereConditions[$key]);
                continue;
            }
            if (0 === strpos($condition, 'AND ')) {
                $condition = preg_replace("/AND /", "", $condition, 1);
            }
            $whereConditions[$key] = $condition;
        }
        if ($whereConditions) {
            $where[] = implode(' AND ', $whereConditions);
            $select->setPart(Select::WHERE, $where);
        }
        $table = $this->getTable('cataloginventory_stock_status');
        $select->joinRight(
            ['inventory' => $table],
            'inventory.product_id = e.entity_id AND inventory.website_id = 0 AND inventory.stock_status = 1',
            []
        );

        return $this->fetchCount($select, $filter);
    }

    /**
     * @param FilterInterface $filter
     * @return array
     */
    public function getParentCount(FilterInterface $filter)
    {
        // Clone select from collection with filters
        $select = clone $filter->getLayer()->getProductCollection()->getSelect();
        $select->reset(Select::WHERE);
        $select->reset(Select::GROUP);

//        $select->joinInner(
//            ['ciss' => $filter->getLayer()->getProductCollection()->getTable('cataloginventory_stock_status')],
//            'e.entity_id = ciss.product_id AND stock_status = 1',
//            []
//        );
        $select->where('stock_status_index.stock_status = 1');

        return $this->fetchCount($select, $filter);
    }

    /**
     * @param Select $select
     * @param FilterInterface $filter
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    private function fetchCount(Select $select, FilterInterface $filter)
    {
        // Reset columns, order and limitation conditions
        $select->reset(Select::COLUMNS)
            ->reset(Select::ORDER)
            ->reset(Select::LIMIT_COUNT)
            ->reset(Select::LIMIT_OFFSET);

        $connection = $this->getConnection();
        $attribute = $filter->getAttributeModel();
        $attributeId = $attribute->getAttributeId();

        //$tableAlias = sprintf('%s_idx', $attribute->getAttributeCode());
        $tableAlias = 'filterable_attributes';
        $flag = 'attribute_joined_' . $tableAlias;
        if (!$filter->getLayer()->getProductCollection()->getFlag($flag)) {

            $conditions = [
                $tableAlias . '.entity_id = e.entity_id',
                //$connection->quoteInto($tableAlias . '.attribute_id = ?', $attribute->getAttributeId()),
                $connection->quoteInto($tableAlias . '.store_id = ?', (int)$filter->getStoreId()),
            ];

            /*$filterableAttributesIds = $this->getFilterableAttributesIds();

            if (!empty($filterableAttributesIds)) {
                if (empty($partWhere)) {
                    //$select->where($tableAlias . '.attribute_id IN (' . implode(',', $filterableAttributesIds) . ')');
                    $select->where($tableAlias . '.attribute_id IN (SELECT attribute_id FROM ' . $this->getTable('catalog_eav_attribute') . ' WHERE is_filterable = 1)');
                }
            } else {
                $conditions[] = $tableAlias . '.attribute_id IN (SELECT attribute_id FROM ' . $this->getTable('catalog_eav_attribute') . ' WHERE is_filterable = 1)';
            }*/

            $select->joinInner(
                [$tableAlias => $this->getMainTable()],
                join(' AND ', $conditions),
                []
            );

            /*$partWhere = $select->getPart(Select::WHERE);
            if (empty($partWhere)) {
                //$select->where($tableAlias . '.attribute_id IN (' . implode(',', $filterableAttributesIds) . ')');
                $select->where($tableAlias . '.attribute_id IN (SELECT attribute_id FROM ' . $this->getTable('catalog_eav_attribute') . ' WHERE is_filterable = 1)');
            }*/

        }

        $select->columns(
            [
                'attribute_id' => $tableAlias . '.attribute_id',
                'value' => $tableAlias . '.value',
                'count' => new \Zend_Db_Expr('COUNT(' . $tableAlias . '.entity_id)')
            ]
        );

        $select->group(["{$tableAlias}.attribute_id","{$tableAlias}.value"]);

        $this->clearSelect($select);

        $cacheId = 'filterable_attributes_' . md5($select);

        if (isset($this->_filterCache[$cacheId])) {
            $data = $this->_filterCache[$cacheId];
        } else {
            $cacheContent = $this->_cacheManager->load($cacheId);
            if ($cacheContent === false) {
                //$select->resetJoinLeft();
                $data = $connection->fetchAll($select);
                $this->_cacheManager->save(serialize($data), $cacheId, [], 60 * 10);
            } else {
                $data = unserialize($cacheContent);
            }

            $this->_filterCache[$cacheId] = $data;
        }

        $result = [];
        foreach ($data as $row) {
            if ($row['attribute_id'] != $attributeId) continue;
            $result[$row['value']] = $row['count'];
        }

        return $result;
        //return $connection->fetchPairs($select);
    }

    protected function getFilterableAttributesIds()
    {
        $cacheId = 'all_filterable_attributes_ids_cache';

        $cacheContent = $this->_cacheManager->load($cacheId);

        if ($cacheContent === false) {
            $connection = $this->getConnection();
            $select = $connection
                ->select()
                ->from(
                    ['eav_attr' => $this->getTable('catalog_eav_attribute')],
                    ['eav_attr.attribute_id']
                )
                ->where('is_filterable = 1');

            $data = $connection->fetchCol($select, 'attribute_id');

            $this->_cacheManager->save(serialize($data), $cacheId, [], 60 * 60 * 2);
        } else {
            $data = unserialize($cacheContent);
        }

        return $data;
    }

    public function getSelectedKeys(FilterInterface $filter)
    {
        $select = clone $filter->getLayer()->getProductCollection()->getSelect();

        $where = $select->getPart(Select::WHERE);
        $result = [];
        $pattern_1 = "/(?>\D)+\(\(/";
        $pattern_2 = "/_idx[\s\S]+/";
        $pattern_3 = "/\(+/";

        foreach ($where as $item)
        {
            $temp = preg_replace($pattern_1, '', $item);
            $temp = preg_replace($pattern_2, '', $temp);
            $temp = preg_replace($pattern_3, '', $temp);
            $result[] = $temp;
        }
        return $result;
    }

    /**
     * @param \Magento\Framework\DB\Select $select
     */
    protected function clearSelect($select) {
        $fromAndJoins = $select->getPart(Select::FROM);

        $select->reset(Select::FROM);

        foreach (self::LEFT_JOIN_TO_EXCLUDE as $joinKey) {
            if (isset($fromAndJoins[$joinKey])) unset($fromAndJoins[$joinKey]);
        }

        $select->setPart(Select::FROM, $fromAndJoins);
    }

}