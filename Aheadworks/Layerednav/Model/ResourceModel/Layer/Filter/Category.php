<?php
namespace Aheadworks\Layerednav\Model\ResourceModel\Layer\Filter;

use Magento\Catalog\Model\Layer\Filter\FilterInterface;
use Magento\Framework\DB\Select;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

/**
 * Class Category
 * @package Aheadworks\Layerednav\Model\ResourceModel\Layer\Filter
 */
class Category extends AbstractDb
{
    /**
     * {@inheritdoc}
     */
    protected function _construct()
    {
        $this->_init('catalog_category_product_index', 'entity_id');
    }

    /**
     * @param FilterInterface $filter
     * @return $this
     */
    public function joinFilterToCollection(FilterInterface $filter)
    {
        $collection = $filter->getLayer()->getProductCollection();
        if (!$collection->getFlag('category_table_joined')) {
            $collection
                ->getSelect()
                ->join(
                    ['cat' => $this->getMainTable()],
                    'cat.product_id = e.entity_id'
                )
            ;
            $collection->setFlag('category_table_joined', true);
        }

        return $this;
    }

    /**
     * Get where conditions
     *
     * @param array $value
     * @return array
     */
    public function getWhereConditions($value)
    {
        $value = implode("','", $value);
        return ["cat.category_id IN ('{$value}')"];
    }
}
