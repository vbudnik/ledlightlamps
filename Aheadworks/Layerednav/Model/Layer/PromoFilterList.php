<?php
namespace Aheadworks\Layerednav\Model\Layer;

use Aheadworks\Layerednav\Model\Config;
use Amasty\Label\Observer\viewBlockAbstractToHtmlBefore;
use Magento\Catalog\Model\Layer;
use Magento\Catalog\Model\Layer\Filter\AbstractFilter;
use Magento\Catalog\Model\Layer\FilterableAttributeListInterface;
use Magento\Catalog\Model\ResourceModel\Eav\Attribute;
use Magento\Framework\ObjectManagerInterface;

/**
 * Class FilterList
 * @package Aheadworks\Layerednav\Model\Layer
 */
class PromoFilterList
{
    // General filters
    const CATEGORY_FILTER = 'category';
    const ATTRIBUTE_FILTER = 'attribute';

    // Special filters
    const PERCENT_FILTER = 'percent';

    /**
     * @var string[]
     */
    private $filterTypes = [
        self::CATEGORY_FILTER => \Aheadworks\Layerednav\Model\Layer\Filter\PromoCategory::class,
        self::ATTRIBUTE_FILTER => \Aheadworks\Layerednav\Model\Layer\Filter\Attribute::class,
        self::PERCENT_FILTER => 'Aheadworks\Layerednav\Model\Layer\Filter\Custom\Percent'
    ];

    /**
     * @var ObjectManagerInterface
     */
    private $objectManager;

    /**
     * @var FilterableAttributeListInterface
     */
    private $filterableAttributes;

    /**
     * @var Config
     */
    private $config;

    /**
     * @var AbstractFilter[]
     */
    private $filters;

    /**
     * @param ObjectManagerInterface $objectManager
     * @param FilterableAttributeListInterface $filterableAttributes
     * @param Config $config
     */
    public function __construct(
        ObjectManagerInterface $objectManager,
        FilterableAttributeListInterface $filterableAttributes,
        Config $config,
        \Magento\Framework\App\ResourceConnection $connectionFactory
    ) {
        $this->objectManager = $objectManager;
        $this->filterableAttributes = $filterableAttributes;
        $this->config = $config;
        $this->_connectionFactory = $connectionFactory;
    }

    /**
     * Get filters
     *
     * @param Layer $layer
     * @return AbstractFilter[]
     */
    public function getFilters(Layer $layer)
    {
        if (!$this->filters) {
            $filters[] = $this->createCustomFilter($layer, self::PERCENT_FILTER);

            $filterableAttributes = $this->filterableAttributes->getList();
            foreach ($filterableAttributes as $attribute) {
                $filters[] = $this->createAttributeFilter($attribute, $layer);
            }

            $this->filters = $filters;
        }

        return $this->filters;
    }

    /**
     * Create attribute filter
     *
     * @param Attribute $attribute
     * @param Layer $layer
     * @return AbstractFilter|false
     */
    private function createAttributeFilter(Attribute $attribute, Layer $layer)
    {
        $filterClassName = $this->filterTypes[self::ATTRIBUTE_FILTER];
        if ($attribute->getAttributeCode() == 'category_ids') {
            $filterClassName = $this->filterTypes[self::CATEGORY_FILTER];
        }

        $filter = $this->objectManager->create(
            $filterClassName,
            ['data' => ['code' => $attribute->getAttributeCode()], 'layer' => $layer]
        );
        return $filter;
    }


    /**
     * Create custom filter
     *
     * @param Layer $layer
     * @param string $filterCode
     * @return AbstractFilter
     */
    private function createCustomFilter(Layer $layer, $filterCode)
    {
        return $this->objectManager->create(
            $this->filterTypes[$filterCode],
            ['data' => ['code' => $filterCode], 'layer' => $layer]
        );
    }
}
