<?php
namespace Aheadworks\Layerednav\Model\Layer;

use Aheadworks\Layerednav\Model\Config;
use Amasty\Label\Observer\viewBlockAbstractToHtmlBefore;
use Magento\Catalog\Model\Layer;
use Magento\Catalog\Model\Layer\Filter\AbstractFilter;
use Magento\Catalog\Model\Layer\FilterableAttributeListInterface;
use Magento\Catalog\Model\ResourceModel\Eav\Attribute;
use Magento\Framework\ObjectManagerInterface;

/**
 * Class FilterList
 * @package Aheadworks\Layerednav\Model\Layer
 */
class FilterList
{
    // General filters
    const CATEGORY_FILTER = 'category';
    const ATTRIBUTE_FILTER = 'attribute';
    const PRICE_FILTER = 'price';
    const DECIMAL_FILTER = 'decimal';

    // Special filters
    const SALES_FILTER = 'sales';
    const NEW_FILTER = 'new';
    const STOCK_FILTER = 'stock';

    //Custom filter for promotion
    const CUSTOM_PROMO_FILTER = 'promo';

    /**
     * @var string[]
     */
    private $filterTypes = [
        self::CATEGORY_FILTER => \Aheadworks\Layerednav\Model\Layer\Filter\Category::class,
        self::ATTRIBUTE_FILTER => \Aheadworks\Layerednav\Model\Layer\Filter\Attribute::class,
        self::PRICE_FILTER => \Aheadworks\Layerednav\Model\Layer\Filter\Price::class,
        self::DECIMAL_FILTER => \Aheadworks\Layerednav\Model\Layer\Filter\Decimal::class,
        self::STOCK_FILTER => 'Aheadworks\Layerednav\Model\Layer\Filter\Custom\Stock',
        self::SALES_FILTER => '\Aheadworks\Layerednav\Model\Layer\Filter\Custom\Sales',
        self::NEW_FILTER => 'Aheadworks\Layerednav\Model\Layer\Filter\Custom\NewProduct',
        self::CUSTOM_PROMO_FILTER => \Aheadworks\Layerednav\Model\Layer\Filter\SpecialPrice::class,
    ];

    /**
     * @var ObjectManagerInterface
     */
    private $objectManager;

    /**
     * @var FilterableAttributeListInterface
     */
    private $filterableAttributes;

    /**
     * @var Config
     */
    private $config;

    /**
     * @var AbstractFilter[]
     */
    private $filters;

    /**
     * @param ObjectManagerInterface $objectManager
     * @param FilterableAttributeListInterface $filterableAttributes
     * @param Config $config
     */
    public function __construct(
        ObjectManagerInterface $objectManager,
        FilterableAttributeListInterface $filterableAttributes,
        Config $config,
        \Magento\Framework\App\ResourceConnection $connectionFactory
    ) {
        $this->objectManager = $objectManager;
        $this->filterableAttributes = $filterableAttributes;
        $this->config = $config;
        $this->_connectionFactory = $connectionFactory;
    }

    /**
     * Get filters
     *
     * @param Layer $layer
     * @return AbstractFilter[]
     */
    public function getFilters(Layer $layer)
    {
        if (!$this->filters) {

            $filters = [];
            $attributeSetIds = [];

            $filters[] = $this->objectManager->create($this->filterTypes[self::CATEGORY_FILTER], ['layer' => $layer]);

            foreach ($this->getAvailableCustomFilters() as $customFilterCode) {
                $filters[] = $this->createCustomFilter($layer, $customFilterCode);
            }

            $connection = $this->_connectionFactory->getConnection();

            $select = $connection->select()
                ->from(
                    ['ccp' => $this->_connectionFactory->getTableName('catalog_category_product')],
                    ['']
                )
                ->joinRight(
                    ['cpe' => $this->_connectionFactory->getTableName('catalog_product_entity')],
                    'ccp.product_id = cpe.entity_id',
                    ['attribute_set_id']
                )
                ->where('ccp.category_id = ?', $layer->getCurrentCategory()->getId())
                //->group('cpe.attribute_set_id');
                ->limit(1);

            $product = $connection->fetchRow($select);
            $attributeSetIds[] = $product['attribute_set_id'];

            /*$productCollection = $layer->getCurrentCategory()->getProductCollection();
            $productCollection->getSelect()->reset(
                \Magento\Framework\DB\Select::COLUMNS
            )->columns('attribute_set_id')->group('attribute_set_id');

            foreach ($productCollection as $product) {
                $attributeSetIds[] = $product['attribute_set_id'];
            }*/

            $filterableAttributes = $this->filterableAttributes
                ->getList(false)
                ->setAttributeSetsFilter($attributeSetIds);

            $filterableAttributes->addExpressionFieldToSelect('position_bool', 'if(position, 1, 0)', 'position');
            $filterableAttributes->unshiftOrder('position_bool','desc');

            foreach ($filterableAttributes as $attribute) {
                $filters[] = $this->createAttributeFilter($attribute, $layer);
            }
            $filters[] = $this->createCustomFilter($layer, self::CUSTOM_PROMO_FILTER);

            $this->filters = $filters;
        }

        return $this->filters;
    }

    /**
     * Create attribute filter
     *
     * @param Attribute $attribute
     * @param Layer $layer
     * @return AbstractFilter
     */
    private function createAttributeFilter(Attribute $attribute, Layer $layer)
    {
        $filterClassName = $this->filterTypes[self::ATTRIBUTE_FILTER];
        if ($attribute->getAttributeCode() == 'price') {
            $filterClassName = $this->filterTypes[self::PRICE_FILTER];
        } elseif ($attribute->getBackendType() == 'decimal') {
            $filterClassName = $this->filterTypes[self::DECIMAL_FILTER];
        } elseif ($attribute->getAttributeCode() == 'category_ids') {
            $filterClassName = $this->filterTypes[self::CATEGORY_FILTER];
        }
        $filter = $this->objectManager->create(
            $filterClassName,
            ['data' => ['attribute_model' => $attribute], 'layer' => $layer]
        );

        return $filter;
    }

    /**
     * Create custom filter
     *
     * @param Layer $layer
     * @param string $filterCode
     * @return AbstractFilter
     */
    private function createCustomFilter(Layer $layer, $filterCode)
    {
        return $this->objectManager->create(
            $this->filterTypes[$filterCode],
            ['layer' => $layer]
        );
    }

    /**
     * Get available custom filter codes
     *
     * @return array
     */
    private function getAvailableCustomFilters()
    {
        $customFilters = [];
        if ($this->config->isNewFilterEnabled()) {
            $customFilters[] = self::NEW_FILTER;
        }
        if ($this->config->isInStockFilterEnabled()) {
            $customFilters[] = self::STOCK_FILTER;
        }
        if ($this->config->isOnSaleFilterEnabled()) {
            $customFilters[] = self::SALES_FILTER;
        }
        return $customFilters;
    }
}
