<?php
namespace Aheadworks\Layerednav\Model\Layer\Filter\Dynamic;

use Aheadworks\Layerednav\Model\ResourceModel\Layer\Filter\Price;
use Magento\Catalog\Model\Layer;
use Magento\Catalog\Model\Layer\Filter\Dynamic\AlgorithmInterface;
use Magento\Catalog\Model\Layer\Filter\Price\Range;
use Magento\Catalog\Model\Layer\Filter\Price\Render;
use Magento\Catalog\Model\Layer\Resolver as LayerResolver;

/**
 * Class Auto
 * @package Aheadworks\Layerednav\Model\Layer\Filter\Dynamic
 */
class Auto implements AlgorithmInterface
{
    const MIN_RANGE_POWER = 10;

    /**
     * @var Layer
     */
    private $layer;

    /**
     * @var Render
     */
    private $render;

    /**
     * @var Range
     */
    private $range;

    /**
     * @var Price
     */
    private $resource;

    /**
     * @param LayerResolver $layerResolver
     * @param Render $render
     * @param Range $range
     * @param Price $resource
     */
    public function __construct(
        LayerResolver $layerResolver,
        Render $render,
        Range $range,
        Price $resource
    ) {
        $this->layer = $layerResolver->get();
        $this->render = $render;
        $this->range = $range;
        $this->resource = $resource;
    }

    /**
     * {@inheritdoc}
     */
    public function getItemsData(array $intervals = [], $additionalRequestData = '')
    {
        $prices = $this->resource->getPrices();

        $minPrice = $prices['min_price'];
        $maxPrice = $prices['max_price'];

        if(!empty($intervals)) {
            $interval = array_shift($intervals);
        } else {
            $interval = [
                $minPrice,
                $maxPrice
            ];
        }
        return [
            [
                'value' => [$minPrice,$maxPrice],
                'label' => 'limits',
                'count' => 1
            ],
            [
                'value' => [$interval[0],$interval[1]?$interval[1]:$maxPrice],
                'label' => 'interval',
                'count' => 1
            ]
        ];
    }

    /**
     * @return number
     */
    private function getRange()
    {
        $maxPrice = $this->getMaxPriceInt();
        return $maxPrice / 4;
    }

    /**
     * Get maximum price from layer products set
     *
     * @return float
     */
    private function getMaxPriceInt()
    {
        $maxPrice = $this->layer->getProductCollection()
            ->getMaxPrice();
        $maxPrice = floor($maxPrice);

        return $maxPrice;
    }
}
