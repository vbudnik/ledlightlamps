<?php
namespace Aheadworks\Layerednav\Model\Layer\Filter;

use Aheadworks\Layerednav\Model\ResourceModel\Layer\Filter\Custom\AbstractFilter as ResourceAbstractFilter;
use Aheadworks\Layerednav\Model\ResourceModel\Layer\ConditionRegistry;
use Magento\Catalog\Model\Layer\Filter\AbstractFilter;
use Magento\Catalog\Model\Layer\Filter\ItemFactory;
use Magento\Catalog\Model\Layer\Filter\Item\DataBuilder as ItemDataBuilder;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Filter\StripTags;
use Magento\Framework\Stdlib\StringUtils;
use Magento\Catalog\Model\Layer\Filter\FilterInterface;
use Magento\Framework\DB\Select;

/**
 * Custom Filter
 * @package Aheadworks\Layerednav\Model\Layer\Filter
 */
class Percent extends AbstractFilter
{

    /**
     * @var ResourceAbstractFilter
     */
    private $resource;

    /**
     * @var StringUtils
     */
    private $stringUtils;

    /**
     * @var StripTags
     */
    private $tagFilter;

    /**
     * @var ConditionRegistry
     */
    private $conditionsRegistry;

    /**
     * @var string
     */
    private $itemLabel;

    /** @var RequestInterface  */
    private $request;

    /**
     * Percent constructor.
     * @param ItemFactory $filterItemFactory
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Catalog\Model\Layer $layer
     * @param ItemDataBuilder $itemDataBuilder
     * @param ConditionRegistry $conditionsRegistry
     * @param ResourceAbstractFilter $resource
     * @param StringUtils $stringUtils
     * @param StripTags $tagFilter
     * @param RequestInterface $request
     * @param $requestVar
     * @param $itemLabel
     * @param array $data
     */
    public function __construct(
        ItemFactory $filterItemFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Catalog\Model\Layer $layer,
        ItemDataBuilder $itemDataBuilder,
        ConditionRegistry $conditionsRegistry,
        ResourceAbstractFilter $resource,
        StringUtils $stringUtils,
        StripTags $tagFilter,
        RequestInterface $request,
        array $data = []
    ) {
        parent::__construct(
            $filterItemFactory,
            $storeManager,
            $layer,
            $itemDataBuilder,
            $data
        );
        $this->request = $request;
        $this->conditionsRegistry = $conditionsRegistry;
        $this->resource = $resource;
        $this->stringUtils = $stringUtils;
        $this->tagFilter = $tagFilter;
        $this->itemLabel = 'Процент скидки, %';
        $this->setRequestVar('percent');
    }

    /**
     * {@inheritdoc}
     */
    public function apply(RequestInterface $request)
    {
        $filter = $request->getParam($this->_requestVar);

        if (is_array($filter)) {
            return $this;
        }

        if ($filter) {
            foreach ($this->resource->getWhereConditions($this, $filter) as $attribute => $conditions) {
                $this->conditionsRegistry->addConditions($attribute, $conditions);
            }
            $this->getLayer()
                ->getState()
                ->addFilter($this->_createItem($this->itemLabel, $filter));
        }
        return $this;
    }

    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    protected function _getItemsData()
    {
        $optionsCount = $this->getCount($this);
        $parentCount = $this->getParentCount($this);
        $options = $this->getOptions();
        foreach (array_keys($parentCount) as $key) {
            $parentCount[$key] = '0';
            if (array_key_exists($key, $optionsCount)) {
                $parentCount[$key] = $optionsCount[$key];
            }
        }
        $optionsCount = $parentCount;

        foreach ($options as $option) {
            if (is_array($option['value'])) {
                continue;
            }
            if ($this->stringUtils->strlen($option['value'])) {
                if (array_key_exists($option['value'], $optionsCount)
                    && ($optionsCount[$option['value']] || $optionsCount[$option['value']] == '0')
                ) {
                    $this->itemDataBuilder->addItemData(
                        $this->tagFilter->filter($option['label']),
                        $option['value'],
                        $optionsCount[$option['value']]
                    );
                }
            }
        }

        return $this->itemDataBuilder->build();
    }

    /**
     * Retrieve array with products counts per attribute option
     *
     * @param FilterInterface $filter
     * @throws \Magento\Framework\Exception\LocalizedException
     * @return array
     */
    public function getCount(FilterInterface $filter)
    {
        $collection = clone $filter->getLayer()->getProductCollection();
        $select = $collection->getSelect();
        $whereConditions = $select->getPart(Select::WHERE);
        $select->reset(Select::WHERE);
        $select->reset(Select::GROUP);

        $tableAlias = 'landing_item';
        foreach ($whereConditions as $key => $condition) {
            if (false !== strpos($condition, $tableAlias)) {
                unset($whereConditions[$key]);
                continue;
            }
            if (0 === strpos($condition, 'AND ')) {
                $condition = preg_replace("/AND /", "", $condition, 1);
            }
            $whereConditions[$key] = $condition;
        }
        if ($whereConditions) {
            $where[] = implode(' AND ', $whereConditions);
            $select->setPart(Select::WHERE, $where);
        }

        return $this->fetchCount($select);
    }

    /**
     * @param FilterInterface $filter
     * @return array
     */
    public function getParentCount(FilterInterface $filter)
    {
        $collection = clone $filter->getLayer()->getProductCollection();
        $select = clone $collection->getSelect();
        $select->reset(Select::WHERE);

        return $this->fetchCount($select);
    }

    private function fetchCount(Select $select)
    {
        // Reset columns, order and limitation conditions
        $select->reset(Select::COLUMNS)
            ->reset(Select::ORDER)
            ->reset(Select::LIMIT_COUNT)
            ->reset(Select::LIMIT_OFFSET)
            ->reset(Select::GROUP)
            ->resetJoinLeft();

        $connection = $this->resource->getConnection();

        $select->columns(
            [
                'value' => new \Zend_Db_Expr('discount_range'),
                'count' => new \Zend_Db_Expr('COUNT(discount_range)')
            ]
        );
        $select->group('discount_range');
        return $connection->fetchPairs($select);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return __($this->itemLabel);
    }

    private function getOptions()
    {
        return \Project\Landing\Model\Page::OPTIONS;
    }
}
