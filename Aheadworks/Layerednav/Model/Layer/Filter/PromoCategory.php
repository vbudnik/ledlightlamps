<?php
namespace Aheadworks\Layerednav\Model\Layer\Filter;

use Aheadworks\Layerednav\Model\Layer\Filter\DataProvider\Category as DataProvider;
use Aheadworks\Layerednav\Model\Layer\Filter\DataProvider\CategoryFactory as DataProviderFactory;
use Aheadworks\Layerednav\Model\PageTypeResolver;
use Aheadworks\Layerednav\Model\ResourceModel\Layer\ConditionRegistry;
use Magento\Catalog\Model\Layer;
use Magento\Catalog\Model\Layer\Filter\AbstractFilter;
use Magento\Catalog\Model\Layer\Filter\ItemFactory;
use Magento\Catalog\Model\Layer\Filter\Item\DataBuilder as ItemDataBuilder;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Escaper;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\DB\Select;

/**
 * Category Filter
 * @package Aheadworks\Layerednav\Model\Layer\Filter
 */
class PromoCategory extends AbstractFilter
{
    /**
     * @var DataProvider
     */
    private $dataProvider;

    /**
     * @var ConditionRegistry
     */
    private $conditionsRegistry;

    /**
     * @var Escaper
     */
    private $escaper;

    /**
     * @var PageTypeResolver
     */
    private $pageTypeResolver;

    /**
     * Cached resources singleton
     *
     * @var \Magento\Framework\App\ResourceConnection
     */
    protected $_resources;

    /**
     * Item Data Builder
     *
     * @var \Aheadworks\Layerednav\Model\Layer\Filter\Item\CategoryDataBuilder
     */
    protected $_categoryItemDataBuilder;

    private $_categoryItems;

    /**
     * @param ItemFactory $filterItemFactory
     * @param StoreManagerInterface $storeManager
     * @param Layer $layer
     * @param ItemDataBuilder $itemDataBuilder
     * @param DataProviderFactory $dataProviderFactory
     * @param ConditionRegistry $conditionsRegistry
     * @param Escaper $escaper
     * @param PageTypeResolver $pageTypeResolver
     * @param array $data
     */
    public function __construct(
        ItemFactory $filterItemFactory,
        StoreManagerInterface $storeManager,
        Layer $layer,
        ItemDataBuilder $itemDataBuilder,
        DataProviderFactory $dataProviderFactory,
        ConditionRegistry $conditionsRegistry,
        Escaper $escaper,
        PageTypeResolver $pageTypeResolver,
        \Aheadworks\Layerednav\Model\Layer\Filter\Item\CategoryDataBuilder $dataBuilder,
        \Magento\Framework\App\ResourceConnection $resourceConnection,
        array $data = []
    ) {
        parent::__construct(
            $filterItemFactory,
            $storeManager,
            $layer,
            $itemDataBuilder,
            $data
        );
        $this->_requestVar = 'cat';
        $this->_resources = $resourceConnection;
        $this->dataProvider = $dataProviderFactory->create(['layer' => $layer]);
        $this->conditionsRegistry = $conditionsRegistry;
        $this->escaper = $escaper;
        $this->pageTypeResolver = $pageTypeResolver;
        $this->_categoryItemDataBuilder = $dataBuilder;
    }

    /**
     * {@inheritdoc}
     */
    public function apply(RequestInterface $request)
    {
        $filter = $request->getParam($this->getRequestVar());
        if (!$filter) {
            return $this;
        }

        $filterParams = explode(',', $filter);
        $categoryIds = $this->dataProvider->validateFilter($filterParams);
        if (!$categoryIds) {
            return $this;
        }

        $this->joinFilterToCollection($this->getLayer()->getProductCollection());
        $this->conditionsRegistry->addConditions(
            'category',
            $this->dataProvider->getResource()->getWhereConditions($categoryIds)
        );

        $this->getLayer()
            ->getState()
            ->addFilter(
                $this->_createItem('category', $filter)
            );

        return $this;
    }

    protected function _initItems()
    {
        $data = $this->_getItemsData();
        $items = [];
        foreach ($data as $itemData) {
            $items[] = $this->_createCategoryItem($itemData['label'], $itemData['value'], $itemData['count'], $itemData['parent_id']);
        }
        $this->_items = $items;
        return $this;
    }

    protected function _createCategoryItem($label, $value, $count = 0, $parentId)
    {
        return $this->_filterItemFactory->create()
            ->setFilter($this)
            ->setLabel($label)
            ->setValue($value)
            ->setCount($count)
            ->setParentId($parentId);
    }

    /**
     * @param \Magento\Catalog\Model\ResourceModel\Product\Collection $collection
     * @return \Magento\Catalog\Model\ResourceModel\Product\Collection
     */
    public function joinFilterToCollection($collection)
    {
        if (!$collection->getFlag('category_table_joined')) {
            $collection
                ->getSelect()
                ->joinInner(
                    ['cat' => $this->_resources->getTableName('catalog_category_product')],
                    'cat.product_id = e.entity_id',
                    ['category_id']
                )->joinInner(
                    ['cat_name' => $this->_resources->getTableName('catalog_category_entity_varchar')],
                    'cat.category_id = cat_name.entity_id and cat_name.attribute_id = 45 and cat_name.store_id = 0',
                    []
                )->joinInner(
                    ['cce' => $this->_resources->getTableName('catalog_category_entity')],
                    'cat.category_id = cce.entity_id',
                    []
                );
            $collection->setFlag('category_table_joined', true);
        }

        return $collection;
    }


    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return __('Category');
    }

    /**
     * {@inheritdoc}
     */
    protected function _getItemsData()
    {
        return $this->getItemsDataForPromoPage();
    }

    public function getCategoryItems($categoryId)
    {
        if (empty($this->_categoryItems)) {
            $items = parent::getItems();
            foreach ($items as $item) {
                $this->_categoryItems[$item['parent_id']][$item->getValue()] = $item;
            }
        }
        return isset($this->_categoryItems[$categoryId])?$this->_categoryItems[$categoryId]:[];
    }

    /**
     * Get filter items data for promo page
     *
     * @return array
     */
    private function getItemsDataForPromoPage()
    {
        $optionsCount = $this->getCount();
        $parentCount = $this->getCount(true);
        $options = $parentCount;

        foreach (array_keys($parentCount) as $key) {
            $parentCount[$key]['count'] = '0';
            if (array_key_exists($key, $optionsCount)) {
                $parentCount[$key] = $optionsCount[$key];
            }
        }
        $optionsCount = $parentCount;
        $this->setParentCategories($parentCount);


        foreach ($options as $option) {
            if (array_key_exists($option['category_id'], $optionsCount)
                && ($optionsCount[$option['category_id']] || $optionsCount[$option['category_id']] == '0')
            ) {
                $this->_categoryItemDataBuilder->addItemData(
                    $option['name'],
                    $option['category_id'],
                    $optionsCount[$option['category_id']]['count'],
                    $option['parent_id']
                );
            }
        }
        return $this->_categoryItemDataBuilder->build();
    }

    private function getCount($parent = false)
    {
        $collection = clone $this->getLayer()->getProductCollection();
        $collection->getSelect()->resetJoinLeft();
        $this->joinFilterToCollection($collection);
        $select = $collection->getSelect();
        if ($parent) {
            $select->reset(Select::WHERE);
        } else {
            $whereConditions = $select->getPart(Select::WHERE);
            $select->reset(Select::WHERE);
            $select->reset(Select::GROUP);

            $tableAlias = 'cat.category_id';
            foreach ($whereConditions as $key => $condition) {
                if (false !== strpos($condition, $tableAlias)) {
                    unset($whereConditions[$key]);
                    continue;
                }
                if (0 === strpos($condition, 'AND ')) {
                    $condition = preg_replace("/AND /", "", $condition, 1);
                }
                $whereConditions[$key] = $condition;
            }
            if ($whereConditions) {
                $where[] = implode(' AND ', $whereConditions);
                $select->setPart(Select::WHERE, $where);
            }
        }
        $select->reset(Select::COLUMNS
        )->reset(
            Select::GROUP
        )->reset(
            Select::ORDER
        )->distinct(
            false
        )->limit();
        $select->group(
            'cat.category_id'
        )->columns([
            'cat.category_id',
            'cat_name.value as name',
            'COUNT(e.entity_id) as count',
            'cce.parent_id'
        ])->order('name');

        $categories = $this->_resources->getConnection()->fetchAssoc($select);
        return $categories;
    }

    private function setParentCategories($categories)
    {
        $parentIds = [];
        foreach ($categories as $category) {
            $parentIds[$category['parent_id']] = $category['parent_id'];
        }

        $connection = $this->_resources->getConnection();
        $select = $connection->select()->from(
            ['cce' => $this->_resources->getTableName('catalog_category_entity')],
            []
        )->joinInner(
            ['cat_name' => $this->_resources->getTableName('catalog_category_entity_varchar')],
            'cce.entity_id = cat_name.entity_id and cat_name.attribute_id = 45 and cat_name.store_id = 0',
            []
        )->where($connection->prepareSqlCondition('cce.entity_id',['in' => $parentIds]));
        $select->columns([
            'cat_name.value as name',
            'cce.entity_id as category_id'
        ]);
        $parentCategories = $connection->fetchAssoc($select);
        ksort($parentCategories);
        $this->setData('parent_categories',$parentCategories);
        return $this;
    }
}
