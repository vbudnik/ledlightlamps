<?php
namespace Aheadworks\Layerednav\Model\Layer\Filter;

use Magento\Catalog\Model\Layer\Filter\AbstractFilter;
use Aheadworks\Layerednav\Model\Layer\Filter\DataProvider\PriceFactory as DataProviderFactory;

/**
 * Class SpecialPrice
 * @package Aheadworks\Layerednav\Model\Layer\Filter
 */
class SpecialPrice extends AbstractFilter
{
    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory
     */
    protected $productCollectionFactory;
    /**
     * @var DataProviderFactory
     */
    protected $dataProvider;
    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * SpecialPrice constructor.
     * @param \Magento\Catalog\Model\Layer\Filter\ItemFactory                $filterItemFactory
     * @param \Magento\Store\Model\StoreManagerInterface                     $storeManager
     * @param \Magento\Catalog\Model\Layer                                   $layer
     * @param \Magento\Catalog\Model\Layer\Filter\Item\DataBuilder           $itemDataBuilder
     * @param \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory
     * @param DataProviderFactory                                            $dataProviderFactory
     * @param \Psr\Log\LoggerInterface                                       $logger
     * @param array                                                          $data
     */
    public function __construct(
        \Magento\Catalog\Model\Layer\Filter\ItemFactory $filterItemFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Catalog\Model\Layer $layer,
        \Magento\Catalog\Model\Layer\Filter\Item\DataBuilder $itemDataBuilder,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        DataProviderFactory $dataProviderFactory,
        \Psr\Log\LoggerInterface $logger,
        array $data = []
    )
    {
        parent::__construct(
            $filterItemFactory,
            $storeManager,
            $layer,
            $itemDataBuilder,
            $data
        );
        $this->productCollectionFactory = $productCollectionFactory;
        $this->_requestVar = 'dis';
        $this->dataProvider = $dataProviderFactory->create();
        $this->logger = $logger;
    }

    /**
     * @return mixed
     */
    public function getResetValue()
    {
        return $this->dataProvider->getResetValue();
    }

    /**
     * @param \Magento\Framework\App\RequestInterface $request
     * @return $this
     */
    public function apply(\Magento\Framework\App\RequestInterface $request)
    {
        $filter = $request->getParam($this->getRequestVar());
        if (!$filter || is_array($filter) || $filter !== 'true') {
            return $this;
        }
        $entity_id = [];
        $collection = $this->productCollectionFactory->create()
            ->addAttributeToSelect(array('sku', 'price', 'special_price', 'special_to_date', 'special_from_date'))
            ->addAttributeToFilter('special_price', array('notnull' => true));

        foreach ($collection as $product) {
            $price = $product->getPrice();
            if($product->getTypeId() == "bundle")
            {
                $price  = $product->getPriceInfo()->getPrice('regular_price')->getAmount()->getValue() ;
            }
            $sprice = $product->getSpecialPrice();
            $sprice_from_date = $product->getSpecialFromDate();
            $sprice_to_date = $product->getSpecialToDate();
            if ($price > 0 &&
                $sprice > 0 &&
                isset($sprice_from_date) &&
                isset($sprice_to_date) &&
                !empty($sprice_from_date) &&
                !empty($sprice_to_date)) {
                if (time() >= strtotime($sprice_from_date) && time() <= strtotime($sprice_to_date)) {
                    $entity_id[] = $product->getId();
                }
            }
        }

        $this->getLayer()
            ->getProductCollection()
            ->addAttributeToFilter('entity_id', array('in' => ($entity_id)));
        return $this;
    }

    /**
     * @return \Magento\Framework\Phrase
     */
    public function getName()
    {
        return __('Акційний товар:');
    }

    /**
     * @return mixed
     */
    protected function _getItemsData()
    {
        $facets = array(
            'true' => '<div class="rating-result"><span>' . __('Супер ціна') . '</span></div>'
        );
        foreach ($facets as $key => $label) {
            $collection = $this->getLayer()->getCurrentCategory()
                ->getProductCollection()
                ->addAttributeToSelect(array('sku', 'price', 'special_price', 'special_to_date', 'special_from_date'))
                ->addFieldToFilter('special_price', array('notnull' => true));
            $count1 = 0;
            foreach ($collection as $product) {
                $price = $product->getPrice();
                if($product->getTypeId() == "bundle") {
                    $price  = $product->getPriceInfo()->getPrice('regular_price')->getAmount()->getValue() ;
                }
                $sprice = $product->getSpecialPrice();
                $sprice_from_date = $product->getSpecialFromDate();
                $sprice_to_date = $product->getSpecialToDate();
                if ($price > 0 &&
                    $sprice > 0 &&
                    isset($sprice_from_date) &&
                    isset($sprice_to_date) &&
                    !empty($sprice_from_date) &&
                    !empty($sprice_to_date)) {
                    if (time() >= strtotime($sprice_from_date) && time() <= strtotime($sprice_to_date)) {
                        $count1++;
                        array($product);
                    }
                }
            }

            if ($count1 > 0) {
                $this->itemDataBuilder->addItemData(
                    $label,
                    $key,
                    $count1
                );
            }
        }
        return $this->itemDataBuilder->build();
    }
}
