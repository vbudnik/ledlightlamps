<?php
/**
 * Created by PhpStorm.
 * User: a4min
 * Date: 08.12.17
 * Time: 13:16
 */

namespace Aheadworks\Layerednav\Model\Layer\Filter\Item;


class CategoryDataBuilder
{
    /**
     * @return array
     */
    protected $_itemsData = [];

    /**
     * Add Item Data
     *
     * @param string $label
     * @param string $label
     * @param int $count
     * @return void
     */
    public function addItemData($label, $value, $count, $parentId)
    {
        $this->_itemsData[] = [
            'label' => $label,
            'value' => $value,
            'count' => $count,
            'parent_id' => $parentId
        ];
    }

    /**
     * Get Items Data
     *
     * @return array
     */
    public function build()
    {
        $result = $this->_itemsData;
        $this->_itemsData = [];
        return $result;
    }

}