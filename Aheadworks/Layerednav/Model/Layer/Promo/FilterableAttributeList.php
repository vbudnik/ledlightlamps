<?php
/**
 * Created by PhpStorm.
 * User: a4min
 * Date: 06.12.17
 * Time: 12:29
 */

namespace Aheadworks\Layerednav\Model\Layer\Promo;


class FilterableAttributeList implements \Magento\Catalog\Model\Layer\FilterableAttributeListInterface
{
    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\Attribute\CollectionFactory
     */
    protected $collectionFactory;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * FilterableAttributeList constructor
     *
     * @param \Magento\Catalog\Model\ResourceModel\Product\Attribute\CollectionFactory $collectionFactory
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     */
    public function __construct(
        \Magento\Catalog\Model\ResourceModel\Product\Attribute\CollectionFactory $collectionFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
        $this->collectionFactory = $collectionFactory;
        $this->storeManager = $storeManager;
    }

    /**
     * @param bool $autoLoad
     * @return \Magento\Catalog\Model\ResourceModel\Product\Attribute\Collection
     */
    public function getList($autoLoad = true)
    {
        /** @var $collection \Magento\Catalog\Model\ResourceModel\Product\Attribute\Collection */
        $collection = $this->collectionFactory->create();
        $collection->setItemObjectClass('Magento\Catalog\Model\ResourceModel\Eav\Attribute')
            ->addStoreLabel($this->storeManager->getStore()->getId());
        $collection->addFieldToFilter('attribute_code',
                [
                    'category_ids'
                ]);
        $collection->addExpressionFieldToSelect('position_bool', 'if(position, 1, 0)', 'position');
        $collection->unshiftOrder('position_bool','desc');
        return $collection;
    }
}