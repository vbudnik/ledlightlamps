<?php
/**
 * Created by PhpStorm.
 * User: a4min
 * Date: 11.08.17
 * Time: 12:06
 */

namespace Aheadworks\Layerednav\Block\Adminhtml;


class AttributesOptionsSorter extends \Magento\Backend\Block\Template
{
    public function __construct(
        \Magento\Backend\Block\Template\Context $context
    )
    {
        parent::__construct($context);
    }

    public function getAutocompleteAttributeUrl() {
        return $this->getUrl('attributes_options_sorter/sorter/autocompleteAttributes');
    }

    public function getAutocompleteAttributeSetUrl() {
        return $this->getUrl('attributes_options_sorter/sorter/autocompleteAttributeSets');
    }

    public function getSorterUrl() {
        return $this->getUrl('attributes_options_sorter/sorter/sorter');
    }
}