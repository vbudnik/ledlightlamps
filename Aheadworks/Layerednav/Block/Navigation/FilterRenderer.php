<?php
namespace Aheadworks\Layerednav\Block\Navigation;

use Magento\Catalog\Model\Layer;
use Magento\Catalog\Model\Layer\Filter\FilterInterface;
use Magento\Catalog\Model\Layer\Filter\Item as FilterItem;
use Magento\Catalog\Model\Layer\Resolver as LayerResolver;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\LayeredNavigation\Block\Navigation\FilterRendererInterface;

/**
 * Class FilterRenderer
 * @package Aheadworks\Layerednav\Block\Navigation\FilterRenderer
 */
class FilterRenderer extends Template implements FilterRendererInterface
{
    /**
     * @var Layer
     */
    private $layer;

    /**
     * @param Context $context
     * @param LayerResolver $layerResolver
     * @param array $data
     */
    public function __construct(
        Context $context,
        LayerResolver $layerResolver,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->layer = $layerResolver->get();
    }

    /**
     * {@inheritdoc}
     */
    public function render(FilterInterface $filter)
    {
        $this->assign('filterItems', $filter->getItems());
        $html = $this->_toHtml();
        $this->assign('filterItems', []);
        return $html;
    }

    /**
     * Check if filter item is active
     *
     * @param FilterItem $item
     * @return bool
     * @throws LocalizedException
     */
    public function isActiveItem(FilterItem $item)
    {
        foreach ($this->layer->getState()->getFilters() as $filter) {
            $filterValues = explode(',', $filter->getValue());
            if ($filter->getFilter()->getRequestVar() == $item->getFilter()->getRequestVar()
                && false !== array_search($item->getValue(), $filterValues)
            ) {
                return true;
            }
        }
        return false;
    }
}
