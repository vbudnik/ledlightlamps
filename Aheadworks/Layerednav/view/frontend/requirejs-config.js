var config = {
    map: {
        '*': {
            awLayeredNavFilterActions:  'Aheadworks_Layerednav/js/filter/actions',
            awLayeredNavFilterItem:     'Aheadworks_Layerednav/js/filter/item',
            awLayeredNavPopover:        'Aheadworks_Layerednav/js/popover',
            awLayeredNavCollapse:       'Aheadworks_Layerednav/js/collapse',
            noUiSlider:                 'Aheadworks_Layerednav/js/nouislider.min',
            productListToolbarForm:     'Aheadworks_Layerednav/js/product-list/toolbar',
            scrollbar: 'Aheadworks_Layerednav/js/enscroll-0.6.2.min',
            filterItem: 'Aheadworks_Layerednav/js/filterItem',
            stickSidebar: 'Aheadworks_Layerednav/js/stick_sidebar'
        }
    }
};
