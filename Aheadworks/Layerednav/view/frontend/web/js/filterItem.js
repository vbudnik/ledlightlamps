    /**
 * Created by m.cherevko on 5/11/2016.
 */

define([
    'jquery',
    'prototype'
], function() {

    var FilterItem = Class.create();

    FilterItem.prototype = {
        initialize: function(element, options) {
            this.element = jQuery(element);
            this.options = options;
        },
        getId: function() {
            return this.element
                .attr('id');
        },
        getTitle: function() {
            if (this.element.attr('name') == 'price') {
                return this.element.data('label');
            }
            return this.element
                .parent()
                .find('.'+this.options.className.itemTitle)
                .text();
        },
        getBlockId: function() {
            return this.element
                .attr('name');
        },
        getBlockTitle: function() {
            return this.element
                .closest('.'+this.options.className.itemBlockContainer)
                .find('.'+this.options.className.itemBlockTitle)
                .text();
        },
        fastContainerElement: function() {
            return jQuery('.'+this.options.className.itemFastBlockContainer);
        },
        fastBlockElement: function() {
            return jQuery('.'+this.options.className.itemFastBlockContainer+' .'+this.getBlockId());
        },
        fastMobileTitleContainer: function () {
            return jQuery(this.element).closest('.'+this.options.className.itemBlockContainer).find('.'+this.options.className.itemMobileTitleContainer);
        },
        isChecked: function() {
            return this.element
                .is(':checked');
        },
        remove: function() {
            this.fastBlockElement().find('.'+this.getId()).remove();
            this.fastMobileTitleContainer().find('.'+this.getId()).remove();
            if (this.fastBlockElement().children('[class^='+this.options.className.itemTitle+']').length < 1) {
                this.fastBlockElement().remove();
            }
        }
    };

    return FilterItem;

});