<?php
/**
 * Created by PhpStorm.
 * User: chere
 * Date: 05.02.2018
 * Time: 16:59
 */

namespace Project\CartridgeSelect\Model;

class AttributeSettingsOptions extends \Magento\Framework\Model\AbstractModel
{
    protected function _construct()
    {
        parent::_construct();
        $this->_init('Project\CartridgeSelect\Model\ResourceModel\AttributeSettingsOptions');
        $this->setIdFieldName('entity_id');
    }

}