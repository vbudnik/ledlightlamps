<?php
namespace Project\CartridgeSelect\Model;

class DataProvider extends \Magento\Ui\DataProvider\AbstractDataProvider
{

    protected $_loadedData;

    /**
     * DataProvider constructor.
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param ResourceModel\AttributeSettings\CollectionFactory $attributeSettingsCollectionFactory
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        \Project\CartridgeSelect\Model\ResourceModel\AttributeSettings\CollectionFactory $attributeSettingsCollectionFactory,
        array $meta = [],
        array $data = []
    ) {
        $this->collection = $attributeSettingsCollectionFactory->create();
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

    /**
     * Get data
     *
     * @return array
     */
    public function getData()
    {
        if (isset($this->_loadedData)) {
            return $this->_loadedData;
        }
        $items = $this->collection->getItems();
        foreach ($items as $item) {
            $this->_loadedData[$item->getId()] = $item->getData();
        }
        return $this->_loadedData;
    }
}