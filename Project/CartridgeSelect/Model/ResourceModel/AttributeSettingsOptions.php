<?php

namespace Project\CartridgeSelect\Model\ResourceModel;

class AttributeSettingsOptions extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        $this->_init('ct_cartridgeselect_attributes_options', 'entity_id');
    }
}