<?php
namespace Project\CartridgeSelect\Model\ResourceModel\AttributeSettings;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected function _construct()
    {
        $this->_init('Project\CartridgeSelect\Model\AttributeSettings', 'Project\CartridgeSelect\Model\ResourceModel\AttributeSettings');
    }
}
