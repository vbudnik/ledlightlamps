<?php

namespace Project\CartridgeSelect\Model\ResourceModel;

class AttributeSettings extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        $this->_init('ct_cartridgeselect_attributes', 'entity_id');
    }
}