<?php
namespace Project\CartridgeSelect\Model\ResourceModel\AttributeSettingsOptions;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected function _construct()
    {
        $this->_init('Project\CartridgeSelect\Model\AttributeSettingsOptions', 'Project\CartridgeSelect\Model\ResourceModel\AttributeSettingsOptions');
    }
}
