<?php
/**
 * Created by PhpStorm.
 * User: chere
 * Date: 29.01.2018
 * Time: 15:27
 */

namespace Project\CartridgeSelect\Controller\Adminhtml\AttributeSettings;

class NewAction extends \Magento\Backend\App\Action
{
    protected $_coreRegistry;

    protected $_attributeSettingsFactory;

    protected $_resultForwardFactory;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Project\CartridgeSelect\Model\AttributeSettingsFactory $attributeSettingsFactory,
        \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory
    ) {
        parent::__construct($context);
        $this->_coreRegistry = $coreRegistry;
        $this->_attributeSettingsFactory = $attributeSettingsFactory;
        $this->_resultForwardFactory = $resultForwardFactory;
    }

    public function execute()
    {
        return $this->_resultForwardFactory->create()->forward('edit');
    }
}