<?php
/**
 * Created by PhpStorm.
 * User: chere
 * Date: 31.01.2018
 * Time: 15:58
 */

namespace Project\CartridgeSelect\Controller\Adminhtml\AttributeSettings;

class Save extends \Magento\Backend\App\Action
{
    /**
     * @var \Project\CartridgeSelect\Model\AttributeSettingsFactory
     */
    protected $_attributeSettingsFactory;

    /**
     * @var \Project\CartridgeSelect\Model\AttributeSettingsOptionsFactory
     */
    protected $_attributeSettingsOptionsFactory;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Project\CartridgeSelect\Model\AttributeSettingsFactory $attributeSettingsFactory,
        \Project\CartridgeSelect\Model\AttributeSettingsOptionsFactory $attributeSettingsOptionsFactory
    ) {
        parent::__construct($context);
        $this->_attributeSettingsFactory = $attributeSettingsFactory;
        $this->_attributeSettingsOptionsFactory = $attributeSettingsOptionsFactory;
    }

    public function execute()
    {
        $data = $this->getRequest()->getPostValue();

        //var_dump($data); die();

        if ($data) {
            $attribute = $this->_attributeSettingsFactory->create();

            if (isset($data['entity_id'])) {
                /** @var  \Project\CartridgeSelect\Model\AttributeSettings $attribute */
                $attribute = $attribute->load($data['entity_id']);
                if (!$attribute || !$attribute->getEntityId()) {
                    $this->messageManager->addErrorMessage(__('Row data no longer exist.'));
                    $this->_redirect('*/*/index');
                    return;
                }
            }

            try {
                $attribute->setName($data['name']);
                $attribute->setIsActive($data['is_active']);
                $attribute->getResource()->save($attribute);

                $options = $this->getRequest()->getParam('option');

                if (isset($options['delete']) && is_array($options['delete'])) {
                    foreach ($options['delete'] as $optionRowId => $deleteStatus) {
                        if ((bool)$deleteStatus == true) {
                            $this->removeOption($attribute, $optionRowId);
                            unset($options['value'][$optionRowId]);
                        }
                    }
                }

                if (isset($options['value']) && is_array($options['value'])) {
                    $this->saveOptions($attribute, $options['value']);
                }

                $this->_redirect('*/*/edit', ['id' => $attribute->getId(), '_current' => true]);
            } catch (\Exception $e) {
                var_dump($e->getMessage());
            }
        }
    }

    protected function removeOption($attribute, $optionRowId)
    {
        $option = $this->_attributeSettingsOptionsFactory->create();

        $existOptionCollection = $option->getCollection()
            ->addFieldToFilter('attribute_id', $attribute->getId())
            ->addFieldToFilter('option_row_id', $optionRowId);

        foreach ($existOptionCollection as $existOption) {
            try {
                $existOption->getResource()->delete($existOption);
            } catch (\Exception $e) {
                var_dump($e->getMessage());
            }
        }
    }

    /**
     * @param \Project\CartridgeSelect\Model\AttributeSettings $attribute
     * @param array $data
     */
    protected function saveOptions($attribute, $data)
    {
        foreach ($data as $optionRowId => $optionData) {

            foreach ($optionData as $storeId => $optionValue) {

                if (empty($optionValue)) continue;

                $option = $this->_attributeSettingsOptionsFactory->create();

                $existOption = $option->getCollection()
                    ->addFieldToFilter('attribute_id', $attribute->getId())
                    ->addFieldToFilter('store_id', $storeId)
                    ->addFieldToFilter('option_row_id', $optionRowId)
                    ->getFirstItem();

                if ($existOption && $existOption->getId()) {
                    $option = $existOption;
                } else {
                    $option->setAttributeId($attribute->getId());
                }

                $option->setOptionRowId($optionRowId);
                $option->setStoreId($storeId);
                $option->setValue($optionValue);

                try {
                    $option->getResource()->save($option);
                } catch (\Exception $e) {
                    var_dump($e->getMessage());
                }
            }
        }
    }
}