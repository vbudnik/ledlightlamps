<?php
/**
 * Created by PhpStorm.
 * User: chere
 * Date: 29.01.2018
 * Time: 15:27
 */

namespace Project\CartridgeSelect\Controller\Adminhtml\AttributeSettings;

class Edit extends \Magento\Backend\App\Action
{
    protected $_coreRegistry;

    protected $_attributeSettingsFactory;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Project\CartridgeSelect\Model\AttributeSettingsFactory $attributeSettingsFactory
    ) {
        parent::__construct($context);
        $this->_coreRegistry = $coreRegistry;
        $this->_attributeSettingsFactory = $attributeSettingsFactory;
    }

    public function execute()
    {
        return $this->resultFactory->create(\Magento\Framework\Controller\ResultFactory::TYPE_PAGE);
    }

//    public function execute()
//    {
//        $title = __('Add Row Data');
//
//        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
//        if ($rowId = (int) $this->getRequest()->getParam('id')) {
//            /** @var \Project\CartridgeSelect\Model\AttributeSettings $rowData */
//            $rowData = $this->_attributeSettingsFactory->create()->load($rowId);
//            if (!$rowData || !$rowData->getEntityId()) {
//                $this->messageManager->addErrorMessage(__('Row data no longer exist.'));
//                $this->_redirect('*/*/index');
//                return;
//            }
//            $this->_coreRegistry->register('row_data', $rowData);
//            $title = __('Edit Row Data ') . $rowData->getTitle();
//        }
//
//        $resultPage = $this->resultFactory->create(\Magento\Framework\Controller\ResultFactory::TYPE_PAGE);
//        $resultPage->getConfig()->getTitle()->prepend($title);
//
//        return $resultPage;
//    }
}