<?php
/**
 *
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Project\CartridgeSelect\Api\Data;

/**
 * @api
 */
interface AttributeSettingsInterface
{
    const ENTITY_ID = 'entity_id';

    const NAME = 'name';

    const IS_ACTIVE = 'is_active';

    public function getEntityId();

    public function getName();

    public function getIsActive();
}
