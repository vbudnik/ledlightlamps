<?php
/**
 * Created by PhpStorm.
 * User: chere
 * Date: 30.01.2018
 * Time: 12:15
 */

namespace Project\CartridgeSelect\Block\Adminhtml;

class AttributeSettings extends \Magento\Backend\Block\Widget\Grid\Container
{
    protected function _construct()
    {
        $this->_controller = 'attributesettings_addrow';
        $this->_blockGroup = 'Project_CartridgeSelect';
        $this->_headerText = __('Attributes');
        $this->_addButtonLabel = __('Create New Attribute');
        parent::_construct();
    }
}