<?php
/**
 * Created by PhpStorm.
 * User: chere
 * Date: 01.02.2018
 * Time: 15:58
 */

namespace Project\CartridgeSelect\Block\Adminhtml\AttributeSettings;

class Options extends \Magento\Backend\Block\Template
{
    protected $_template = 'attribute_settings/options.phtml';
    protected $_attributeSettingsOptionsFactory;

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Project\CartridgeSelect\Model\AttributeSettingsOptionsFactory $attributeSettingsOptionsFactory,
        array $data = []
    ) {
        $this->_attributeSettingsOptionsFactory = $attributeSettingsOptionsFactory;
        parent::__construct($context, $data);
    }

    public function getStores()
    {
        if (!$this->hasStores()) {
            $this->setData('stores', $this->_storeManager->getStores(true));
        }
        return $this->_getData('stores');
    }

    public function getStoresSortedBySortOrder()
    {
        $stores = $this->getStores();
        if (is_array($stores)) {
            usort($stores, function ($storeA, $storeB) {
                if ($storeA->getSortOrder() == $storeB->getSortOrder()) {
                    return $storeA->getId() < $storeB->getId() ? -1 : 1;
                }
                return ($storeA->getSortOrder() < $storeB->getSortOrder()) ? -1 : 1;
            });
        }
        return $stores;
    }

    public function getOptionValues()
    {
        $result = $resultTmp = [];

        if ($attributeId = $this->getRequest()->getParam('id')) {
            $options = $this->_attributeSettingsOptionsFactory->create();
            $optionCollection = $options->getCollection()
                ->addFieldToFilter('attribute_id', $attributeId);

            foreach ($optionCollection as $option) {
                $resultTmp[$option->getOptionRowId()][$option->getStoreId()] = $option->getValue();
            }
        }

        foreach ($resultTmp as $optionRowId => $optionData) {
            $result[$optionRowId]['checked'] = '';
            $result[$optionRowId]['intype'] = 'radio';
            $result[$optionRowId]['sort_order'] = $optionRowId;
            $result[$optionRowId]['id'] = $optionRowId;
            foreach ($optionData as $storeId => $optionValue) {
                $result[$optionRowId]['store'.$storeId] = $optionValue;
            }
        }

        return $result;
    }
}