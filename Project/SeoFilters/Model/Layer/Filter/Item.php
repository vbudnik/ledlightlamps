<?php

namespace Project\SeoFilters\Model\Layer\Filter;

class Item extends \Magento\Catalog\Model\Layer\Filter\Item
{
    /**
     * Get filter item url
     *
     * @return string
     */
    public function getFilterUrl()
    {
        $query = [
            $this->getFilter()->getRequestVar() => $this->getValue(),
            // exclude current page from urls
            $this->_htmlPagerBlock->getPageVarName() => null,
        ];
        return $this->_url->getUrl('*/*/*', ['_current' => false, '_use_rewrite' => true, '_query' => $query]);
    }
}