<?php
namespace Project\SeoFilters\Model\ResourceModel\FilterSettings;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected function _construct()
    {
        $this->_init('Project\SeoFilters\Model\FilterSettings', 'Project\SeoFilters\Model\ResourceModel\FilterSettings');
    }
}
