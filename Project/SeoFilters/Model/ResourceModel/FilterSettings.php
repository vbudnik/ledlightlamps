<?php

namespace Project\SeoFilters\Model\ResourceModel;

class FilterSettings extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        $this->_init('ct_seo_filters_attributes', 'entity_id');
    }
}