<?php

namespace Project\SeoFilters\Model;

class FilterSettings extends \Magento\Framework\Model\AbstractModel implements \Project\CartridgeSelect\Api\Data\AttributeSettingsInterface
{
    protected function _construct()
    {
        parent::_construct();
        $this->_init('Project\SeoFilters\Model\ResourceModel\FilterSettings');
        $this->setIdFieldName('entity_id');
    }

    public function getEntityId()
    {
        return $this->getData(self::ENTITY_ID);
    }

    public function getName()
    {
        return $this->getData(self::NAME);
    }

    public function getIsActive()
    {
        return $this->getData(self::IS_ACTIVE);
    }
}